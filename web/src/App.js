import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import './App.css'
import Footer from './components/layout/Footer'
import Home from './components/pages/Home'
import IEDC from './components/pages/IEDC'
import Menu from './components/layout/Menu'

const App = () => {
	return (
		<Router>
			<Menu />
			<Switch>
				<Route exact path='/' component={Home} />
				<Route exact path='/iedc' component={IEDC} />
			</Switch>
			<Footer />
		</Router>
	)
}

export default App
