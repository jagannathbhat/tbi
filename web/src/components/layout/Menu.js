import React, { useEffect, useState } from 'react'
import { withRouter, Link } from 'react-router-dom'

import './Menu.css'

import initialMenuItems from '../menuItems.json'

const Menu = ({ history, location }) => {
	useEffect(() => {
		history.listen(location => {
			setTimeout(() => {
				window.animation()
				// eslint-disable-next-line
				pathname = location.pathname
				setMenuItems(FilterMenuItems)
			}, 500)
		})
	})

	const FilterMenuItems = () =>
		initialMenuItems.filter(item => item.link !== pathname)

	const LinkPressed = ({ target }) => {
		pathname = target.getAttribute('href')
		MenuToggle()
		setTimeout(() => {
			window.animation()
			setMenuItems(FilterMenuItems)
		}, 500)
		window.scrollTo(0, 0)
	}

	const MenuToggle = () => setMenuState(MenuState === 'menu' ? 'close' : 'menu')

	let pathname = location.pathname

	const [MenuState, setMenuState] = useState('menu')

	const [MenuItems, setMenuItems] = useState(FilterMenuItems)

	return (
		<>
			<div className={MenuState} id='menu-btn' onClick={MenuToggle}>
				<div className='bar' id='bar1'></div>
				<div className='bar' id='bar2'></div>
				<div className='bar' id='bar3'></div>
			</div>
			<div className={MenuState} id='drawer'>
				<ul>
					{MenuItems.map(({ link, name }, index) => (
						<li key={index}>
							<Link to={link} onClick={LinkPressed}>
								{name}
							</Link>
						</li>
					))}
				</ul>
			</div>
		</>
	)
}

export default withRouter(Menu)
