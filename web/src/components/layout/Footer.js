import React from 'react'
import { Link } from 'react-router-dom'

import './Footer.css'
import MenuItem from '../menuItems.json'

const Footer = () => {
	const LinkPressed = () => {
		window.scrollTo(0, 0)
		window.animation()
	}

	return (
		<footer>
			<div>
				<h3>Links</h3>
				<ul>
					{MenuItem.map(({ link, name }, index) => (
						<li key={index}>
							<Link to={link} onClick={LinkPressed}>
								{name}
							</Link>
						</li>
					))}
				</ul>
			</div>
			<div>
				<h3>Contact</h3>
				<p>
					Vidya Bharathi Nagar, Mattoor,
					<br />
					Kalady, Ernakulam District,
					<br />
					Kerala State Pin: 683574
				</p>
				<p>
					<a href='mailto:tbi@adishankara.ac.in'>tbi@adishankara.ac.in</a>
				</p>
			</div>
			{/*<p id='credits'>
				Icons from: https://www.flaticon.com/authors/darius-dan,
				https://www.flaticon.com/authors/smalllikeart,
				https://www.flaticon.com/authors/pixelmeetup,
				https://www.flaticon.com/authors/freepik
			</p>*/}
		</footer>
	)
}

export default Footer
