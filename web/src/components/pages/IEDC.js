import React from 'react'

import './IEDC.css'
import Achievement from '../achievements/Achievement'
import achievements from '../achievements.json'

const IEDC = () => {
	return (
		<>
			<section id='bannerIEDC'>
				<h1 className='anim animAppear'>
					Adi Shankara
					<br />
					<span className='green'>IEDC</span>
				</h1>
			</section>
			<section id='about'>
				<p className='anim animAppear'>
					Adi Shankara Institute of Engineering and Technology proved to be the
					stage for yet another exciting student venture through the
					inauguration of Kerala Startup Mission's Innovation and
					Entrepreneurship Development Centre from 10th February 2015.
				</p>
				<p className='anim animAppear'>
					Named as
					<span className='blue'> Adi Shankara IEDC Boot Camp (ABC)</span>, the
					chapter aims at improving the entrepreneurship and technical skills of
					passionate students and at the same time, coming out with produces
					which would have the potential of generating revenue. Adi Shankara
					IEDC Bootcamp is working its best to spread the message of
					<br />
					<span className='green'>
						Innovation, Incubation, and Entrepreneurship
					</span>
					<br />
					among students in the college/region.
				</p>
				<h2>
					<span className='blue'>Pre-Incubation</span> Facility
				</h2>
				<div className='underline anim'></div>
				<p className='anim animAppear'>
					We also have a pre-incubation facility with 1435sqft. space for our
					student startups. This Co-Workin.g Space is being used by our students
					who have registered startups. The space consist of office furniture,
					white board, wi-fi and table tennis board too!
				</p>
			</section>
			<section id='achievements'>
				<h2 className='anim animAppear'>Major IEDC Achievements</h2>
				<div className='underline anim'></div>
				{achievements.map((achievement, index) => (
					<Achievement key={index} {...achievement} />
				))}
			</section>
		</>
	)
}

export default IEDC
