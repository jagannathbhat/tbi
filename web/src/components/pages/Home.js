import React from 'react'

import './Home.css'
import startups from '../startups.json'

const Home = () => (
	<>
		<section id='banner' className='centerizer'>
			<div style={{ width: '100%', overflow: 'hidden' }}>
				<h1 className='anim animFade'>
					Adi Shankara <span className='green'>TBI</span>
				</h1>
				<p className='anim animAppear'>
					Automation of traditional systems by technology upgradation
				</p>
				<img src='images/building.jpg' alt='asiet iedc office' />
			</div>
		</section>
		<section id='awards'>
			<div className='award'>
				<p className='anim animAppear'>
					Best IEDC Chapter <span className='blue'>2017</span>
				</p>
				<div className='anim underline'></div>
			</div>
			<div className='award'>
				<p className='anim animAppear'>
					Best IEDC Chapter <span className='blue'>2018</span>
				</p>
				<div className='anim underline'></div>
			</div>
			<div className='award'>
				<p className='anim animAppear'>
					Best IEDC Chapter <span className='blue'>2019</span>
				</p>
				<div className='anim underline'></div>
			</div>
			<h1 className='title anim animFade'>Three years in a row!</h1>
		</section>
		<section id='enabler' className='centerizer'>
			<h2 className='anim animAppear'>
				<small>winner of</small>
				<br />
				<span className='green'>Entrepreneurship Enabler</span>
				<br />
				Award 2018
			</h2>
			<p className='anim animAppear'>
				for the notable contributions to the startup ecosystem
			</p>
		</section>
		<section id='bhub'>
			<div className='wrap'>
				<h2 className='anim animAppear'>
					TBI
					<br />
					<small className='green'>Technology Business Incubator</small>
				</h2>
				<p className='anim animAppear'>
					Evaluating the performance of our IEDC chapter, in 2019, Kerala State
					Industrial Development Cooperation (KSIDC) has sanctioned the
					Technology Business Incubation Centre at Adi shankara.
				</p>
				<p className='anim animAppear'>
					Currently, 10 of our alumni startups got incubated at TBI. Apart from
					various acceleration programs and startup funding mechanisms run by
					the TBI, The institution has also decided to provide a Seed Funding
					support of upto 5 Lakhs to all potential startups for their initial
					product development.
				</p>
			</div>
		</section>
		<section id='mission' className='centerizer'>
			<p className='anim animAppear'>
				We are planning to take up the entrepreneurial journey by focusing more
				on automation of traditional systems by technology upgradation through
				which the upliftment of rural people can be made possible.
			</p>
			<div>
				<div className='anim animAppear field'>
					<img alt='robotics' src='images/robotics.svg' />
					<p className='blue'>Robotics and Industrial IoT</p>
				</div>
				<div className='anim animAppear field'>
					<img alt='rural' src='images/rural.svg' />
					<p className='green'>Rural Technology</p>
				</div>
				<div className='anim animAppear field'>
					<img alt='ai' src='images/ai.svg' />
					<p className='blue'>AI & ML in Healthcare</p>
				</div>
				<div className='anim animAppear field'>
					<img alt='green' src='images/green.svg' />
					<p className='green'>Green Technology</p>
				</div>
			</div>
		</section>
		<section id='startups'>
			<div className='startup' id='title'>
				<p>
					Our
					<br />
					Startups
				</p>
			</div>
			{startups.map(({ img, name }, index) => (
				<div
					className='startup anim animFade'
					key={index}
					style={{
						background: `white url(logos/${img}) center/contain no-repeat`,
					}}
				>
					<div className='text'>
						<p>{name}</p>
					</div>
				</div>
			))}
		</section>
	</>
)

export default Home
