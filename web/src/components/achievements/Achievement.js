import React from 'react'

const Achievement = ({ date, desc, title }) => {
	return (
		<div className='anim animAppear card'>
			<div className='card-content'>
				<h3>{title}</h3>
				<p className='card-date'>{date}</p>
				<p>{desc}</p>
			</div>
		</div>
	)
}

export default Achievement
